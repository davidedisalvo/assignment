import Vuex from 'vuex'

const createStore = () => {
	return new Vuex.Store({
		state: () => ({
			startPlay: false
		}),
		mutations: {
			playerFound(state) {
				this.state.startPlay = true
			},
			resetContdown(state) {
				this.state.startPlay = false
			}
		}
	})
}

export default createStore
